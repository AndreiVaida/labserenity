package LabSerenity.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://localhost:3000/")
public class AddÎntrebarePage extends PageObject {
    @FindBy(name = "enunt")
    private WebElementFacade enunțInput;

    @FindBy(name = "varianta1")
    private WebElementFacade varianta1Input;

    @FindBy(name = "varianta2")
    private WebElementFacade varianta2Input;

    @FindBy(name = "variantaCorecta")
    private WebElementFacade variantaCorectaInput;

    @FindBy(name = "domeniu")
    private WebElementFacade domeniuInput;

    @FindBy(id = "addQuestionButton")
    private WebElementFacade addQuestionButton;

    @FindBy(id = "textToShow")
    private WebElementFacade textToShowDiv;

    public void inputEnunțTerm(final String enunțTerm) {
        element(enunțInput).waitUntilVisible();
        enunțInput.sendKeys(enunțTerm);
    }

    public void inputVarianta1Term(final String varianta1) {
        element(varianta1Input).waitUntilVisible();
        varianta1Input.sendKeys(varianta1);
    }

    public void inputVarianta2Term(final String varianta2) {
        element(varianta2Input).waitUntilVisible();
        varianta2Input.sendKeys(varianta2);
    }

    public void inputVariantaCorectaTerm(final String variantaCorecta) {
        element(variantaCorectaInput).waitUntilVisible();
        variantaCorectaInput.sendKeys(variantaCorecta);
    }

    public void inputDomeniuTerm(final String domeniu) {
        element(domeniuInput).waitUntilVisible();
        domeniuInput.sendKeys(domeniu);
    }

    public void clickAddQuestionButton() {
        element(addQuestionButton).waitUntilVisible();
        addQuestionButton.click();
    }

    public String getTextToShow() {
        element(textToShowDiv).waitUntilVisible();
        return textToShowDiv.getText();
    }
}

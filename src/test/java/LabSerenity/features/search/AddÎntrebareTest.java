package LabSerenity.features.search;

import LabSerenity.steps.serenity.AddÎntrebareSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.Pages;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@RunWith(SerenityRunner.class)
public class AddÎntrebareTest {
    private static final String COMMA_DELIMITER = ",";
    private static final String FILE_NAME = "întrebări.csv";
    private final Random random = new Random();

    @Managed(uniqueSession = true)
    WebDriver webDriver;

    @ManagedPages(defaultUrl = "http://localhost:3000/")
    public Pages pages;

    @Steps
    public AddÎntrebareSteps addÎntrebareSteps;

    @Test
    public void addÎntrebareTest_valid() throws IOException {
        webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        final IntrebareDto întrebare = loadÎntrebăriFromFile().get(0);
        întrebare.enunt = generateRandomEnunț();
        addÎntrebareSteps.addQuestion(întrebare.enunt, întrebare.varianta1, întrebare.varianta2, întrebare.variantaCorecta, întrebare.domeniu);
        addÎntrebareSteps.outputMessageShouldContains("Succes");
    }

    @Test
    public void addÎntrebareTest_invalid() throws IOException {
        webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        final IntrebareDto întrebare = loadÎntrebăriFromFile().get(1);
        addÎntrebareSteps.addQuestion(întrebare.enunt, întrebare.varianta1, întrebare.varianta2, întrebare.variantaCorecta, întrebare.domeniu);
        addÎntrebareSteps.outputMessageShouldContains("Eșuat");
    }

    private String generateRandomEnunț() {
        int randomNumber = random.nextInt(10000);
        return  "Întrebare " + randomNumber + " ?";
    }

    private List<IntrebareDto> loadÎntrebăriFromFile() throws IOException {
        final List<IntrebareDto> intrebareList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
            String line = br.readLine();
            while ((line = br.readLine()) != null) {
                final String[] values = line.split(COMMA_DELIMITER);
                final String enunț = values[0];
                final String varianta1 = values[1];
                final String varianta2 = values[2];
                final String variantaCorectă = values[3];
                final String domeniu = values[4];
                intrebareList.add(new IntrebareDto(enunț, varianta1, varianta2, variantaCorectă, domeniu));
            }
        }
        return intrebareList;
    }
}

class IntrebareDto {
    String enunt;
    String varianta1;
    String varianta2;
    String variantaCorecta;
    String domeniu;

    public IntrebareDto() {
    }

    public IntrebareDto(String enunt, String varianta1, String varianta2, String variantaCorecta, String domeniu) {
        this.enunt = enunt;
        this.varianta1 = varianta1;
        this.varianta2 = varianta2;
        this.variantaCorecta = variantaCorecta;
        this.domeniu = domeniu;
    }
}

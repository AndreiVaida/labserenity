package LabSerenity.features.search;

import LabSerenity.steps.serenity.AddÎntrebareSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/WikiTestData.csv")
public class AddÎntrebareTest_file {
    private static final String COMMA_DELIMITER = ",";
    private static final String FILE_NAME = "întrebări.csv";
    private final Random random = new Random();

    public String valid;
    public String enunt;
    public String varianta1;
    public String varianta2;
    public String variantaCorecta;
    public String domeniu;

    @Managed(uniqueSession = true)
    WebDriver webDriver;

    @ManagedPages(defaultUrl = "http://localhost:3000/")
    public Pages pages;

    @Steps
    public AddÎntrebareSteps addÎntrebareSteps;

    @Test
    public void addÎntrebareTest_valid() {
        if (valid.equals("truee")) {
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            addÎntrebareSteps.addQuestion(generateRandomEnunț(), varianta1, varianta2, variantaCorecta, domeniu);
            addÎntrebareSteps.outputMessageShouldContains("Succes");
        }
    }

    @Test
    public void addÎntrebareTest_invalid() {
        if (valid.equals("falsee")) {
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            addÎntrebareSteps.addQuestion(enunt, varianta1, varianta2, variantaCorecta, domeniu);
            addÎntrebareSteps.outputMessageShouldContains("Eșuat");
        }
    }

    private String generateRandomEnunț() {
        int randomNumber = random.nextInt(10000);
        return "Întrebare " + randomNumber + " ?";
    }

    @Qualifier
    public String getQualifier() {
        return enunt;
    }

    public String getValid() {
        return valid;
    }
    public void setValid(String isValid) {
        this.valid = isValid;
    }

    public String getEnunt() {
        return enunt;
    }
    public void setEnunt(String enunt) {
        this.enunt = enunt;
    }

    public String getVarianta1() {
        return varianta1;
    }
    public void setVarianta1(String varianta1) {
        this.varianta1 = varianta1;
    }

    public String getVarianta2() {
        return varianta2;
    }
    public void setVarianta2(String varianta2) {
        this.varianta2 = varianta2;
    }

    public String getVariantaCorecta() {
        return variantaCorecta;
    }
    public void setVariantaCorecta(String variantaCorecta) {
        this.variantaCorecta = variantaCorecta;
    }

    public String getDomeniu() {
        return domeniu;
    }
    public void setDomeniu(String domeniu) {
        this.domeniu = domeniu;
    }
}

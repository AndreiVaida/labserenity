package LabSerenity.steps.serenity;

import LabSerenity.pages.AddÎntrebarePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.StepGroup;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Assert;

public class AddÎntrebareSteps extends ScenarioSteps {
    AddÎntrebarePage addÎntrebarePage;

    @Step
    public void openAddÎntrebarePage() {
        addÎntrebarePage.open();
    }

    @Step
    public void writeEnunț(String enunț) {
        addÎntrebarePage.inputEnunțTerm(enunț);
    }

    @Step
    public void writeVarianta1(String varianta1) {
        addÎntrebarePage.inputVarianta1Term(varianta1);
    }

    @Step
    public void writeVarianta2(String varianta2) {
        addÎntrebarePage.inputVarianta2Term(varianta2);
    }

    @Step
    public void writeVariantaCorecta(String variantaCorecta) {
        addÎntrebarePage.inputVariantaCorectaTerm(variantaCorecta);
    }

    @Step
    public void writeDomeniu(String domeniu) {
        addÎntrebarePage.inputDomeniuTerm(domeniu);
    }

    @Step
    public void clickAddQuestionButton() {
        addÎntrebarePage.clickAddQuestionButton();
    }

    @Step
    public void outputMessageShouldContains(String string) {
        Assert.assertTrue(addÎntrebarePage.getTextToShow().contains(string));
    }

    @StepGroup
    public void addQuestion(String enunț, String varianta1, String varianta2, String variantaCorecta, String domeniu) {
        openAddÎntrebarePage();
        writeEnunț(enunț);
        writeVarianta1(varianta1);
        writeVarianta2(varianta2);
        writeVariantaCorecta(variantaCorecta);
        writeDomeniu(domeniu);
        clickAddQuestionButton();
    }
}
